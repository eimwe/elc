# C++ Quiz

[![npoint](https://img.shields.io/badge/n%3A-JSONbin-blue)](https://api.npoint.io/83ef935a4e807fa235ec)

C++ Quiz SPA made with Vue.js 3 framework and JSON data fetched externally.

## Try the quiz

You can try this quiz out at [GitLab Pages](https://elc-eimwe-28d5ed9b451d17b5e0a3350c7a13ed9d5ef11528412a6cf90db84.gitlab.io/)

## Technology stack

- Vue 3 Composition API
- Vue Router
- Dart Sass
- JavaScript ES8+
- Vite

## Test environment

| Browser         | Version |
| --------------- | ------- |
| Google Chrome   | 123     |
| Mozilla Firefox | 122     |

## Usage instructions

1. Clone the repository
2. Run `npm ci` to install dependencies
3. Run one of the following commands:

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Compile and Minify for Production

```sh
npm run build
```

## Attributions

1. [Laith Academy](https://www.youtube.com/@laithacademy) for the detailed tutorial on how to create quiz app from [this YT video](https://youtu.be/I_xLMmNeLDY);
2. [Kitty](https://kittygiraudel.com/) for the 7-1 sass files organizing pattern [described in this article](https://sass-guidelin.es/#architecture);
3. [Nicolas Gallagher](https://nicolasgallagher.com/) for the _normalize.css_ file that you can [get from this source](https://necolas.github.io/normalize.css/);
4. [easyScript](https://stackoverflow.com/users/2390645/easyscript) for a hint on turning on the css sourcemaps in [their stackoverflow reply](https://stackoverflow.com/a/73648558) licensed by https://creativecommons.org/licenses/by-sa/4.0/;
5. [tony19](https://stackoverflow.com/users/6277151/tony19) for the example of Rollup config to change output file directories described in [their post](https://stackoverflow.com/a/71190586) licensed by https://creativecommons.org/licenses/by-sa/4.0/;
6. [mahan](https://stackoverflow.com/users/8292178/mahan) for taking the above solution a little bit further and giving an example of distributing fonts in a separate output folder in [this post](https://stackoverflow.com/a/72024201) licensed by https://creativecommons.org/licenses/by-sa/4.0/;
7. [Kevin Powell](https://www.kevinpowell.co/) for the inspirational and highly informative pieces of advice on how to make the corners of the Internet just a little bit more awesome.

## Contributing

If you get interested in this project, I'd be glad to see your contributions. Pull requests are welcome!
